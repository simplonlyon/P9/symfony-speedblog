<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));
            $user->setRole('ROLE_USER');

            $manager->persist($user);
            $manager->flush();
            return $this->redirectToRoute('login');
            
        }

        return $this->render('auth/index.html.twig', [
            'form' => $form->createView()
        ]);
    }   

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $utils) {

        return $this->render('auth/login.html.twig', [
            'error' => $utils->getLastAuthenticationError(),
            'username' => $utils->getLastUsername()
        ]);
    }

    
}
