<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Post;
use App\Form\PostType;
use App\Service\UploadService;
use Symfony\Component\HttpFoundation\Response;

class MyBlogController extends AbstractController
{
    /**
     * @Route("/my-blog/add/{post}", name="my_blog")
     */
    public function index(Request $request, ObjectManager $manager, UploadService $service, Post $post = null)
    {
        
        if(!$post) {
            $post = new Post();
        } else {
            if($post->getAuthor() != $this->getUser()) {
                return new Response('You don\'t have permission to delete this post', 401);
            }
        }

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setDate(new \DateTime());
            $post->setAuthor($this->getUser());
            /**
             * On utilise notre service d'upload auquel on donne à
             * manger l'image uploadée par le formulaire et on assigne
             * le retour de la méthode upload à la propriété image
             * de l'entité post
             */
            if ($post->getFileImage()) {
                $post->setImage($service->upload($post->getFileImage()));
            }

            $manager->persist($post);
            $manager->flush();
        }

        return $this->render('my_blog/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/my-blog/manage", name="my_blog_manage")
     */
    public function manage()
    {

        return $this->render('read_blog/one-blog.html.twig', [
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/my-blog/delete/{id}", name="my_blog_delete")
     */
    public function delete(ObjectManager $manager, Post $post)
    {
        if ($this->getUser() === $post->getAuthor()) {
            $manager->remove($post);
            $manager->flush();
            return $this->redirectToRoute('my_blog_manage');
        } else {
            return new Response('You don\'t have permission to delete this post', 401);
        }
    }
}
