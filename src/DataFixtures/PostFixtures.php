<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Service\UploadService;
use App\Entity\Post;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Symfony\Component\Filesystem\Filesystem;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    private $uploader;
    private $filesystem;

    /**
     * On injecte le service qui fait l'upload des fichier ainsi que 
     * le Filesystem qui nous permettra de supprimer/copier des fichiers
     * plus simplement
     */
    public function __construct(UploadService $uploader, Filesystem $filesystem)
    {
        $this->uploader = $uploader;
        $this->filesystem = $filesystem;
    }

    public function load(ObjectManager $manager)
    {
        /**
         * On supprime le dossier upload à chaque fixtures histoire de 
         * pas se retrouver avec un dossier uploads contenant un million
         * de fois la même image au fil des loads
         */
        $this->filesystem->remove($_ENV['UPLOAD_DIRECTORY']);

        //On fait une petite boucle pour ajouter des post à 5 users
        for ($y = 1; $y < 5; $y++) {
            //On chope la référence à chaque user sur l'autre fixture
            $user = $this->getReference(UserFixtures::USER_REFERENCE.$y);
            //On fait une boucle pour faire 5 post par user
            for($x = 1; $x <= 5; $x++) {

                $post = new Post();
                $post->setTitle('title '.$x);
                $post->setContent('du contenu contennu contenu contenu contenu '.$x);
                $post->setDate(new \DateTime('2019-10-0'.$x));
                $post->setAuthor($user);
                //On duplique notre image de fixture que l'uploader va renommer et déplacer
                $this->filesystem->copy(__DIR__ . '/../../assets/fixtures/img1.jpg', __DIR__ . '/../../assets/fixtures/imgtemp.jpg');
                //On déclenche l'upload de l'image
                $image = $this->uploader->upload(new File(__DIR__ . '/../../assets/fixtures/imgtemp.jpg'));
                $post->setImage($image);
                $manager->persist($post);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class
        ];
    }
}
