# SpeedBlog
Un petit blog fait avec symfony avec la Promo 9 de simplon Lyon

## How To Use
1. Cloner le projet
2. Faire un `composer install`
3. Créer un .env.local à la racine avec `DATABASE_URL=mysql://username:password@127.0.0.1:3306/db_name` en remplaçant par vos infos de connexion
4. Faire un `bin/console doctrine:schema:drop --force` puis un `bin/console doctrine:database:create` puis un `bin/console doctrine:migrations:migrate`
5. Faire un  `bin/console doctrine:fixtures:load` pour peupler la bdd avec les données de test
6. Faire un `npm install` puis un `npm run build` pour installer les assets gérés par webpack

## Instructions
### I. Création du projet
1. Créer un nouveau projet symfony avec le website-skeleton que s'appelerions symfony-speed-blog
2. Créer le .env.local avec les informations de connexion dedans
### II. Authentification
1. Créer une entité User (avec le make:entity) qui aura une propriété username en string, password en string et role en string
2. Implémenter l'interface UserInterface dessus et toutes ses méthodes (faire que le getRoles fasse un return de [$this->role])
3. Faire le doctrine:database:create puis le  make migration et le migrations migrate
4. Configurer le security.yaml comme on l'a fait dans le projet symfony-security (attention, l'identifiant de notre notre User est sa propriété username cette fois, pas email)
    * Faire le provider pour l'entité
    * Faire l'encoder pour l'entité en utilisant argon2i comme algorithme
    * Faire la partie form_login dans le main
    * Faire la partie logout dans le main (et configurer la route de logout dans le routes.yaml)
5. Créer un form pour le User (avec make:form), retirer le add('role') dans la classe UserType
6. Créer un controller AuthController (avec make:controller)
7. Dans ce controller utiliser la route index générée par défaut pour en faire le register, sur la route /register et qui > créera le formulaire > handle la request > dans le if on encode le password comme dans le projet d'hier puis on fait un setRole('ROLE_USER') sur le user en plus > on fait ensuite persister le user > on affiche le formulaire dans le template
8. Créer une nouvelle route dans le controller qui s'appelera /login et dedans, faire le formulaire de login tel qu'on l'a fait hier (ça sera le même)
9. Dans le base.html.twig, ajouter un lien vers le register, un lien vers le login, un lien vers le logout (comme on a fait hier, avec les conditions et tout)
10. Respirer.... c'est bon, l'authentification est faite

### III. Faire des articles
1. Créer une entity Post qui aura comme propriété : title en string, content en text, date en DateTime, et author en relation ManyToOne avec la classe User
2. Faire le make migration / migrations migrate
3. Créer un form pour le Post, puis dans la classe PostType générée, retirer le ->add('author') et le ->add('date')
4. Créer un controller MyBlogController
5. Faire que la première route générée pointe sur /my-blog/add et dedans faire un formulaire pour le PostType, avec un persist et un flush
6. A l'intérieur du if submit/valid de la route, faire un $post->setDate(new \DateTime()) et un $post->setAuthor($this->getUser()) avant le persist
(Pour faire en sorte que lorsqu'on rajoute un nouvel article, la date soit la date de maintenant et l'auteur.ice soit le user actuellement connecté.e)
7. Faire le template du form, classique, rien de particulier
8. Aller dans le security.yaml dans la partie access_control, faire en sorte que tout ce qui commence par /my-blog ne soit accessible qu'aux personnes avec le role ROLE_USER
### IV. Affichage des trucs
1. Créer un nouveau controller ReadBlogController
2. Dans ce controller faire que la première route pointe sur /
3. Dans cette route, faire en sorte de récupérer tous les users avec le UserRepository et les exposer au template
4. Toujours dans le controller, faire une nouvelle route oneBlog qui pointera sur /blog/{id} et injécter le user dans les argument de la route (avec le ParamConverter)
5. Faire en sorte, avec le PostRepository, de récupérer tous les Post fait par le user récupéré par l'id et exposer les au template
Il faudra utiliser un findBy ou bien directement le getPosts() sur le user
6. Faire le template de la route / où on affichera tous les users avec un for sous forme de lien qui pointera sur /blog/id_du_user
7. Faire le template de la route /blog/id qui affichera tous les Post avec une boucle, sous forme de cards par exemple
8. Faire une nouvelle route onePost dans le controller qui pointera sur /post/{id} et injecter l'entité Post (ParamConverter toussa) pour l'exposer au template
9. Faire le template du one-post où on affichera juste le post en question au format que vous voulez
10. Modifier le template du one-blog pour faire rajouter sur chaque article un lien vers la route one-post
### VII. Espace Perso
1. Dans le controller MyBlogController, rajouter une route sur /my-blog/manage
2. Dans le template de cette route, faire une boucle sur les posts du user actuellement connecté
app.user.posts
3. Toujours dans le MyBlogController, rajouter une route sur /my-blog/delete/{id} qui utilisera le ObjectManager pour supprimer le Post en question puis faire une redirection vers my-blog/manage
4. Rajouter un lien delete dans la boucle de votre template my-blog-manage qui pointera sur la route de suppression
5. Créer une route /my-blog/modify/{id} qui utilisera le formulaire PostType pour mettre à jour le Post récupéré par le paramconverter
(à priori c'est la même logique controleur que celui pour l'ajout, donc à voir si vous voulez faire une seule route pour les deux ou pas)